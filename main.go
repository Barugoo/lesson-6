package main

import (
	"errors"
	"log"
	"sync"
	"time"
)

// This is a FILO implementation of the association list

type List struct {
	mx   sync.RWMutex
	Head *Node
	Tail *Node
}

type NodeData struct {
	SiteName string
	URL      string
}

type Node struct {
	Data     NodeData
	Next     *Node
	Previous *Node
}

// Push adds a new node to an association list
func (list *List) Push(nodeData NodeData) error {
	list.mx.Lock()
	defer list.mx.Unlock()

	// TODO: Validation must be moved outside the function
	if nodeData.SiteName == "" || nodeData.URL == "" {
		return errors.New("incorrect node data")
	}

	var node = Node{Data: nodeData}

	switch list.Head {
	case nil: // list is empty
		list.Head = &node
		list.Tail = &node
	default: // list is NOT empty
		node.Previous = list.Tail
		list.Tail.Next = &node
		list.Tail = &node
	}

	return nil
}

// Pop takes a node in the list, the taken nodes is deleted from the list after the function call
func (list *List) Pop() (node *Node, err error) {
	list.mx.Lock()
	defer list.mx.Unlock()

	if list.Head != nil {
		if list.Head.Next != nil {
			list.Head.Next.Previous = nil
		}

		node = list.Head
		list.Head = list.Head.Next
		return
	}

	// TODO: Pop() must return an additional parameter to detect the case below
	return nil, errors.New("there is no node in the list anymore")
}

func (list *List) Print() {
	list.mx.RLock()
	defer list.mx.RUnlock()

	switch list.Head {
	case nil:
		log.Println("The list is empty")
		return
	default:

		var node *Node
		node = list.Head

		log.Println("=== The list will be printed ===")

	MainLoop:
		for {

			log.Printf("%s (%s)", node.Data.SiteName, node.Data.URL)

			if node.Previous != nil {
				log.Printf("- Previous: %s", node.Previous.Data.SiteName)
			}

			switch node.Next {
			case nil:
				log.Println("=== Printing finished ===")
				break MainLoop
			default:
				log.Printf("- Next: %s", node.Next.Data.SiteName)
				node = node.Next
				time.Sleep(time.Second)
			}
		}
	}
}

func main() {
	var (
		list List
		err  error
	)

	// trying to pop from the empty list
	_, err = list.Pop()
	if err != nil {
		log.Println(err)
		return
	}

	// adding a node to the list
	err = list.Push(NodeData{"Yandex", "ya.ru"})
	if err != nil {
		log.Println(err)
		return
	}

	list.Print()

	err = list.Push(NodeData{"Google", "google.ru"})
	if err != nil {
		log.Println(err)
		return
	}

	err = list.Push(NodeData{"Mail", "mail.ru"})
	if err != nil {
		log.Println(err)
		return
	}

	err = list.Push(NodeData{"Rambler", "rambler.ru"})
	if err != nil {
		log.Println(err)
		return
	}

	list.Print()

	var aNode *Node
	aNode, err = list.Pop()
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("the popped node:", aNode.Data.SiteName)
	list.Print()
}
